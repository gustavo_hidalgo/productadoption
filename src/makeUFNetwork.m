function [Network, numAgents] = makeUFNetwork()
% Download
Epinions1Data = UFget('SNAP/soc-Epinions1');

% Extract the sparse matrix representing who-trusts-whom
Network = Epinions1Data.A;

% Note the number of agents in this network
numAgents = min (size (Network));


end

